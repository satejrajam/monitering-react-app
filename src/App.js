import React, { Component } from 'react';
import './App.css';
import Login from './Components/Login';
import { Route, Switch } from 'react-router-dom';
import Dashboard from './Components/Dashboard';
import MoniteringLive from './Components/MoniteringLive';
import { HashRouter as Router } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router
        // basename="/"
      >
        <div className="appRoot">
          <Switch>
            <Route exact path="/" component={Login} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/dashboard/" component={Dashboard} />
            <Route exact path="/dashboard/:id" component={Dashboard} />
            <Route exact path="/monitering-live" component={MoniteringLive} />
            <Route path="/*" component={Login} />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
