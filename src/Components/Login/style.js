export default {
    rootLogin:{
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        background: 'linear-gradient(to right, #e0eafc, #cfdef3)'
    },
    textField:{
        width: 256
    },
    loginBtn:{
        width: '100%',
        marginTop: 30
    },
    loginBtnWrap:{
        width: 256
    },
    logoWrap:{
        textAlign: 'center',    
        "& img":{
            maxWidth: '100%',
            width: 200
        }
    },
    loginFormWrap:{
        boxShadow: '0 4px 5px 0 #d3d3d3',
        borderRadius: 10,
        padding: '35px 20px',
        background: '#fff'  
    }
}