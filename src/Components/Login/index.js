import React, { Component } from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import injectSheet from 'react-jss'
import style from './style'

let path = '../../';

@injectSheet(style)
export default class Login extends Component {
  render() {
    let { classes } = this.props
    return (
      <div className={classes.rootLogin}>
        <div className={classes.loginFormWrap}>        
          <div className={classes.logoWrap}>
            <img src={path + 'assets/img/logo-mitkat.png'} alt="logo" />
          </div>
          <div>
            <TextField
              id="username"
              label="Username or Email"
              className={classes.textField}
              // value={this.state.name}
              // onChange={this.handleChange('name')}
              margin="normal"
            />
          </div>
          <div>
            <TextField
              id="password-input"
              label="Password"
              className={classes.textField}
              type="password"
              autoComplete="current-password"
              margin="normal"
            />
          </div>
          <div className={classes.loginBtnWrap}>
            <Button variant="contained" color="primary" className={classes.loginBtn}>
              Login
            </Button>
          </div>
        </div>
      </div>
    )
  }
}
