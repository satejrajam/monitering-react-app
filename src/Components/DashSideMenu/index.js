import React, { Component } from 'react'
import MenuList from '@material-ui/core/MenuList';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import style from './style';
import injectSheet from 'react-jss';

@injectSheet(style)
export default class DashSideMenu extends Component {
  render() {

    let { classes, isLive } = this.props;

    return (
      <div className={classes.rootDashSideMenu}>
        <MenuList>
            {
                !isLive 
                ?
                <div>
                    <Link to="/dashboard/map">
                        <MenuItem classes={{root: classes.menuItem}}>
                            <span className="font-wrap">
                                <i className="fa fa-map-o" aria-hidden="true"></i>
                            </span>
                            <span>Map</span>
                        </MenuItem>
                    </Link>
                    <Link to="/dashboard/trending">
                        <MenuItem classes={{root: classes.menuItem}}>
                            <span className="font-wrap">
                                <i className="fa fa-line-chart" aria-hidden="true"></i>
                            </span>
                            <span>Trending</span>                    
                        </MenuItem>
                    </Link>
                    <Link to="/dashboard/monitering">
                        <MenuItem classes={{root: classes.menuItem}}>
                            <span className="font-wrap">
                                <i className="fa fa-tachometer" aria-hidden="true"></i>
                            </span>
                            <span>Monitering</span>                    
                        </MenuItem>
                    </Link>
                </div>
                :
                <div>
                    <Link to="/dashboard">
                        <MenuItem classes={{root: classes.menuItem}}>
                            <span className="font-wrap">
                                <i class="fa fa-home" aria-hidden="true"></i>
                            </span>
                            <span>Home</span>                    
                        </MenuItem>
                    </Link>
                    <Link to="/monitering-live">
                        <MenuItem classes={{root: classes.menuItem}}>
                            <span className="font-wrap">
                                <i className="fa fa-tachometer" aria-hidden="true"></i>
                            </span>
                            <span>Monitering</span>                    
                        </MenuItem>
                    </Link>
                </div>
             }   
        </MenuList>
      </div>
    )
  }
}
