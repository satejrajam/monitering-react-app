export default {
    rootDashSideMenu:{

    },
    menuItem:{
        color: 'rgba(255, 255, 255, 0.8)',
        height: 80,
        justifyContent: 'center',
        display: 'flex',
        flexDirection: 'column',
        "& .font-wrap":{
            paddingBottom: 3,
            "& .fa":{
                fontSize: '1.2em'
            }
        },
        '&:focus':{
            color: '#fff',
            backgroundColor: '#e74c3c',
            // fontWeight: 600,
        },
        "& a":{
            outline: 'none',
            textDecoration: 'none'
        }
    }
}