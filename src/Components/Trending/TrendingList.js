import React, { Component } from 'react'
import injectSheet from 'react-jss'
import style from './style'
import TrendingListItem from './TrendingListItem';
import Title from '../ui/Title';

@injectSheet(style)
export default class TrendingList extends Component {
  render() {

    let TrendingListXML = [];

    return (
      <div className="list-item-wrap">
        <Title title={"Trending"} fontType={"fa-line-chart"} />
        <TrendingListItem />
        <TrendingListItem />
        <TrendingListItem />
        <TrendingListItem />
        <TrendingListItem />
        <TrendingListItem />
      </div>
    )
  }
}
