import React, { Component } from 'react'
import injectSheet from 'react-jss'
import style from './style'
import TrendingList from './TrendingList'

@injectSheet(style)
export default class Trending extends Component {
  render() {

    let { classes } = this.props;

    return (
      <div className={classes.rootTrending}>
        <TrendingList />
      </div>
    )
  }
}
