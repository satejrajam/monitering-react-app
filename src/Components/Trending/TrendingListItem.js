import React, { Component } from 'react'
import injectSheet from 'react-jss'
import style from './style'

@injectSheet(style)
export default class TrendingListItem extends Component {
  render() {
    return (
      <div className="trend-list-item">
        <div className="trend-title">
          <div className="title-wrap">
            <h1>Alert Title goes here</h1>
          </div>
          <div className="icon-wrap">
            <span><i class="fa fa-info-circle" aria-hidden="true"></i></span>
          </div>
        </div>
        <div className="trend-desc">
          <p>alert description goes here</p>
        </div>
      </div>
    )
  }
}
