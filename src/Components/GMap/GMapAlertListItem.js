import React, { Component } from 'react'
import style from './style'
import injectSheet from 'react-jss'

@injectSheet(style)
export default class GMapAlertListItem extends Component {
  render() {

    let { classes } = this.props;

    return (
      <div className={classes.rootGMapListItem}>        
        <div>
          <h1>GMapAlertListItem title</h1>
        </div>
        <div>
          <p>GMapAlertListItem desc</p>
        </div>
      </div>
    )
  }
}
