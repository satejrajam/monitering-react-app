import React, { Component } from 'react'
import injectSheet from 'react-jss'
import style from './style'

const AlertItem = () => (
  <div className="alert-item">
    <div className="alert-item-title"><h2>title</h2></div>
    <div className="alert-item-desc"><p>desc</p></div>
    <div className="alert-item-date"><small>date</small></div>
  </div>
)

@injectSheet(style)
export default class GMapAlertList extends Component {
  render() {

    let { classes } = this.props

    return (
      <div className={classes.rootGMapAlertList}>
        <AlertItem />
        <AlertItem />
        <AlertItem />
      </div>
    )
  }
}
