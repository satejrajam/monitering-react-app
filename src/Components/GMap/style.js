export default {
    rootGMapAlertCompRoot:{
        display: "flex",
        minHeight: '100vh',
        "& .alertListWrap":{
            flexBasis: '320px',
            borderRight: '1px solid #d3d3d3'
            // backgroundColor: 'yellow'
        },
        "& .alertListItemWrap":{
            flex: 1,
            // backgroundColor: 'grey'
        }
    },
    rootGMapAlertList:{
        "& .alert-item":{
            padding: 10,
            background: '#fff',
            borderBottom: '1px solid #d3d3d3',
            cursor: 'pointer',
            "& .alert-item-title":{
                paddingBottom: 8,
                "& h2":{
                    textTransform: 'capitalize',
                    fontWeight: 600,
                    color: '#444',
                    fontSize: '1em',
                    margin: 0                    
                }
            },
            "& .alert-item-desc":{
                paddingBottom: 8,
                "& p":{
                    fontSize: '0.9em',
                    color: '#444',
                    margin: 0
                }
            },
            "& .alert-item-date":{

            },
            "&:hover":{
                backgroundColor: '#f3f3f3'
            }
        }
    },
    rootGMapListItem:{
        padding: '10px 15px',

        "& h1":{
            fontSize: '1.5em',
            color: '#444'
        },
        "& p":{
            color: '#444',
            fontSize: '1em'
        }
    }
}