import React, { Component } from 'react'
import GMapAlertList from './GMapAlertList'
import GMapAlertListItem from './GMapAlertListItem'
import style from './style'
import injectSheet from 'react-jss'

@injectSheet(style)
export default class GMapAlertComponent extends Component {
  render() {

    let { classes } = this.props;

    return (
      <div className={classes.rootGMapAlertCompRoot}>
        <div className="alertListWrap">
            <GMapAlertList />
        </div>
        <div className="alertListItemWrap">
            <GMapAlertListItem />
        </div>
      </div>
    )
  }
}
