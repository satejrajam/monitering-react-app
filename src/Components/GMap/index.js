import React, { Component } from 'react'
import Title from '../ui/Title'
import GMapAlertComponent from './GMapAlertComponent'
import MapApi from './MapApi'

export default class GMap extends Component {


  state = {
    showMap: true
  }

  render() {

    let { showMap } = this.state;
    
    return (
      <div>
        <Title title={"Map Alerts"} fontType={"fa-map-o"} />
          <div className="map-wrapper">
              {
                showMap 
                ?
                <MapApi />
                :
                <GMapAlertComponent />
              }
          </div>
      </div>
    )
  }
}
