export default {
    rootLayout:{
        display: 'flex',
        "& .side-menu-wrap":{
            position: 'fixed',
            backgroundColor: '#34495e',
            flexBasis: '100px',
            minHeight: '100vh',
            // padding: '10px',
            color: 'rgba(255,255,255,0.7)'
        },
        "& .dash-content-wrap":{
            flex: 1,
            marginLeft: 109,
            "& .dash-content-innner":{
                minHeight: '100vh',
                backgroundColor: 'rgba(236, 240, 241, 0.3)'
                // background: 'linear-gradient(to right, #e0eafc, #cfdef3)'
            }
        }
    }
}