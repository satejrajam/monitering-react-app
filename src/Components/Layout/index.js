import React, { Component } from 'react'
import DashHeader from '../DashHeader'
import DashSideMenu from '../DashSideMenu'
import GMap from '../GMap'
import Monitering from '../Monitering';
import Trending from '../Trending';
import { Route, Switch } from 'react-router-dom';
import injectSheet from 'react-jss';
import style from './style';
import MoniteringItemsLive from '../MoniteringLive/MoniteringItemsLive';

@injectSheet(style)
export default class Layout extends Component {
  render() {

    let { classes, isLive } = this.props;

    return (
      <div className={classes.rootLayout}>
          <div className="side-menu-wrap">
            <DashSideMenu isLive={isLive ? isLive : false} />
          </div>
          <div className="dash-content-wrap">
            <DashHeader /> 
            <div className="dash-content-innner">
                {
                  !isLive
                  ?
                  <Switch>
                    <Route path="/dashboard/trending" component={Trending} />
                    <Route path="/dashboard/monitering" component={Monitering} />
                    <Route path="/*" component={GMap} />
                  </Switch>
                  :
                  <MoniteringItemsLive />
                }                
            </div>
          </div>
        
      </div>
    )
  }
}
