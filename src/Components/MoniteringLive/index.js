import React, { Component } from 'react'
import Layout from '../Layout'

export default class MoniteringLive extends Component {
  render() {
    return (
      <div>
        <Layout isLive={true} />
      </div>
    )
  }
}
