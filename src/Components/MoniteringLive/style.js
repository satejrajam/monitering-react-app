export default {
    rootAlertTypes:{
        padding: 35,
        background: '#fff',
        borderBottom: '1px solid #d3d3d3',
        display: 'flex',
        justifyContent: 'center',
        "& .btn-wrap":{
            margin: '0 20px 0'
        }
    },
    subjectTxt:{
        width: '100%'
    },
    messageTxt:{
        width: '100%'
    },
    sendBtn:{
        width: '100%',
    },
    btnWrap:{
        marginTop: 15
    },
    sendIcon:{
        marginLeft: 10
    },
    rootEmailForm:{
        padding: '0 30px 30px'
    }
}