import React, { Component } from 'react'
import AlertTypes from './AlertTypes'
import style from './style'
import injectSheet from 'react-jss'
import Card from '../ui/Card'
import EmailModal from './EmailModal';

@injectSheet(style)
export default class MoniteringItemsLive extends Component{

    state = {
        show: false
    }

    handleEmail = () => {
        this.setState({show: true})
    }
    
    handleClose = () => {
        this.setState({show: false})
    }

    render(){

        let { classes } = this.props;
        let { show } = this.state;

        return(
            <div className={classes.rootLiveMonitering}>
                <AlertTypes handleEmail={this.handleEmail} />
                <div className="monitering-live-wrapper">
                    <Card />
                </div>
                <EmailModal show={show} handleClose={this.handleClose} />
            </div>
        )
    }
}