import React, { Component } from 'react'
import CustomModal from '../ui/CustomModal'
import TextField from '@material-ui/core/TextField';
import style from './style'
import injectSheet from 'react-jss'
import Button from '@material-ui/core/Button';
import SendIcon from '@material-ui/core/Icon';
import Title from '../ui/Title'

@injectSheet(style)
export default class EmailModal extends Component {
  render() {
    let {show, handleClose, classes } = this.props;
    
    return (
      <div>
        <CustomModal show={show} handleClose={handleClose}>
            <div>
              <Title title={'Send Alert'} fontType="fa-envelope" />
            </div>
            <div className={classes.rootEmailForm}>
              <div>
                <TextField
                  id="subject"
                  label="Subject"
                  className={classes.subjectTxt}
                  // value={this.state.subject}
                  // onChange={this.handleChange('subject')}
                  margin="normal"
                  />
              </div>
              <div>
                <TextField
                  id="message"
                  label="Message"
                  multiline
                  rows="4"
                  defaultValue="Enter Content..."
                  className={classes.messageTxt}
                  margin="normal"
                />
              </div>
              <div className={classes.btnWrap}>
              <Button variant="contained" color="secondary" className={classes.sendBtn}>
                Submit
                <span className={classes.sendIcon}>
                  <i class="fa fa-paper-plane" aria-hidden="true"></i>
                </span>
              </Button>
              </div>
            </div>
        </CustomModal>
      </div>
    )
  }
}
