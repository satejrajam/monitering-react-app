import React, { Component } from 'react'
import style from './style'
import injectSheet from 'react-jss'
import Button from '@material-ui/core/Button';

@injectSheet(style)
export default class AlertTypes extends Component {

  render() {

    let { classes, handleEmail } = this.props;

    return (
      <div className={classes.rootAlertTypes}>
        <div className="btn-wrap">
          <Button variant="raised" disabled className="">
          SMS
          </Button>
        </div>
        <div className="btn-wrap">
          <Button variant="raised" color="primary" className="" onClick={() => handleEmail()}>
            Email
          </Button>
        </div>
        <div className="btn-wrap">
          <Button variant="raised" disabled color="secondary" className="">
            Archive
          </Button>
        </div>
      </div>
    )
  }
}
