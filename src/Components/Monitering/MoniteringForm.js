import React, { Component } from 'react'
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import injectSheet from 'react-jss'
import FormControl from '@material-ui/core/FormControl';
import Button from '@material-ui/core/Button';
import style from './style'
import { Link } from 'react-router-dom';

@injectSheet(style)
export default class MoniteringForm extends Component {

    state = {
        age: '',
        name: 'hai',
      };
    
  render() {

    let { classes } = this.props;

    return (
      <div className={classes.rootMoniteringForm}>
            <div className="event-monitering-title">
                <h1>Event Monitoring</h1>
            </div>
          <div className="select-wrap">
          <FormControl className="formControl">
            <InputLabel htmlFor="age-simple">Country</InputLabel>
            <Select
                value={this.state.age}
                onChange={this.handleChange}
                inputProps={{
                name: 'age',
                id: 'age-simple',
                }}
            >
                <MenuItem value="">
                <em>None</em>
                </MenuItem>
                <MenuItem value={10}>India</MenuItem>
            </Select>
            </FormControl>
          </div>
          <div className="select-wrap">
            <FormControl className="formControl">
                <InputLabel htmlFor="age-simple">Zone</InputLabel>
                <Select
                    value={this.state.age}
                    onChange={this.handleChange}
                    inputProps={{
                    name: 'age',
                    id: 'age-simple',
                    }}
                >
                    <MenuItem value="">
                    <em>None</em>
                    </MenuItem>
                    <MenuItem value={10}>South Asia</MenuItem>                    
                </Select>
                </FormControl>
          </div>
          <div className="select-wrap">
            <FormControl className="formControl">
                <InputLabel htmlFor="age-simple">City</InputLabel>
                <Select
                    value={this.state.age}
                    onChange={this.handleChange}
                    inputProps={{
                    name: 'age',
                    id: 'age-simple',
                    }}
                >
                    <MenuItem value="">
                    <em>None</em>
                    </MenuItem>
                    <MenuItem value={10}>Mumbai</MenuItem>
                    <MenuItem value={20}>Delhi</MenuItem>
                </Select>
                </FormControl>
          </div>
          <div className="select-wrap moniter-btn-wrap">
            <Link to="/monitering-live" target="_blank">
                <Button variant="contained" color="secondary">
                    Monitor
                </Button>
            </Link>
          </div>
      </div>
    )
  }
}
