export default {
    rootMonitering:{

    },
    rootMoniteringForm:{
        // minHeight: '100vh',
        padding: 35,
        width: 320,
        // height: 512,
        boxShadow: '0 0 15px 0 #d3d3d3',

        "& .select-wrap":{
            marginBottom: 30,
            "&.moniter-btn-wrap":{
                textAlign: 'center',
                marginBottom: 0
                // width: '100%'
            }
        },
        "& .formControl":{
            minWidth: 320
        },
        "& .event-monitering-title":{
            marginBottom: 15,
            "& h1":{
                fontSize: '1.2em',
                color: '#27ae60'
            }
        }
    },
    moniteringForm:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        // minHeight: '100vh',
        background: '#fff',
        paddingTop: 60
    }
}