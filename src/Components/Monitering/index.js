import React, { Component } from 'react'
import Title from '../ui/Title';
import MoniteringForm from './MoniteringForm';
import injectSheet from 'react-jss'
import style from './style'

@injectSheet(style)
export default class Monitering extends Component {
  render() {

    let { classes } = this.props;

    return (
      <div className={classes.rootMonitering}>
        <Title title={"Customize Monitoring"} fontType={"fa-tachometer"} />
        <div className={classes.moniteringForm}>
          <MoniteringForm />
        </div>
      </div>
    )
  }
}
