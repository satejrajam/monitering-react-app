export default {
    rootDashHead:{
        position: 'sticky',
        top: 0,
        background: '#fff',
        padding: 30,
        boxShadow: '0 8px 6px -6px #d3d3d3',
        // boxShadow: '0 0 6px -0 #d3d3d3',
        // borderBottom: '1px solid #d3d3d3', 
        zIndex: 99,
        "& .settings-wrap":{
            position: 'relative',
            height: 20,
            display: 'flex',
            alignItems: 'center',
            '& .sign-out':{
                cursor: 'pointer',
                // position: 'absolute',
                right: 0,
                "& .fa":{
                    fontSize: '1.2em',
                    color: '#444'
                }
            },
            "& .logo-wrap":{
                flex: 1,
                "& img":{
                    maxWidth: '100%',
                    width: 148
                }
            },
            "& .setting-inner-wrap":{
                flex: 1,
                display: 'flex',
                justifyContent: 'flex-end'
            }
        }
    }
}