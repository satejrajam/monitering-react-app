import React, { Component } from 'react'
import style from './style'
import injectSheet from 'react-jss'

let path = process.env === "development" ? "../../" : "/";

@injectSheet(style)
export default class DashHeader extends Component {
  render() {

    let { classes } = this.props;

    return (
      <div className={classes.rootDashHead}>
        <div className="settings-wrap">
          <div className="logo-wrap">
            <img src={path + 'assets/img/logo-mitkat.png'} alt="logo" />
          </div>
          <div className="setting-inner-wrap">
            <span className="sign-out" onClick={() => {console.log('logout clicked!')}}>
              <i className="fa fa-sign-out" aria-hidden="true"></i>
            </span>
          </div>
        </div>
      </div>
    )
  }
}
