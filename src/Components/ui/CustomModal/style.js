export default {
    rootCustomModal:{
        
    },
    modalOuter:{
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
        width: 600,
        backgroundColor: '#fff',
        boxShadow: '0 0 1px 0 #d3d3d3',
        // padding: 30,
        borderRadius: 3
    }
}