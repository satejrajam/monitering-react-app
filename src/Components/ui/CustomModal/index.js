import React, { Component } from 'react'
import Modal from '@material-ui/core/Modal';
import injectSheet from 'react-jss'
import style from './style'

@injectSheet(style)
export default class CustomModal extends Component {

  render() {

    let { show, children, classes, handleClose } = this.props;

    return (
      <div className={classes.rootCustomModal}>
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={show}
                onClose={() => handleClose()}
            >
                <div className={classes.modalOuter}>
                    {children}
                </div>
            </Modal>        
      </div>
    )
  }
}
