import React, { Component } from 'react'
import style from './style'
import injectSheet from 'react-jss'

@injectSheet(style)
export default class Card extends Component {
  render() {
      let { classes } = this.props
    return (      
        <div className={classes.rootCard}>
            <div className="trend-list-item">
                <div className="trend-title">
                    <div className="title-wrap">
                        <h1>Alert Title goes here</h1>
                    </div>
                    <div className="icon-wrap">
                        <span><i class="fa fa-info-circle" aria-hidden="true"></i></span>
                    </div>
                </div>
                <div className="trend-desc">
                    <p>alert description goes here</p>
                </div>
            </div>
        </div>      
    )
  }
}
