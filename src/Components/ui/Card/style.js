export default {
    rootCard:{
        "& .trend-list-item":{
            padding: '10px 35px',
            cursor: 'pointer',
            // backgroundColor: '#f5f6fa',
            backgroundColor: '#fff',
            boxShadow: '0 1px 1px 0 #d3d3d3',
            margin: '20px 35px',
            borderRadius: 2,
            border: '1px solid #d3d3d3',
            '&:hover':{
                backgroundColor: '#f3f3f3'
            }
        },
        "& .trend-list-item:nth-child(even)":{
            backgroundColor: '#fff',
            '&:hover':{
                backgroundColor: '#f3f3f3'
            }
        },
        "& .trend-title":{
            display: 'flex',
            alignItems: 'center',
            "& .title-wrap":{
                flex: 1
            },
            "& .icon-wrap":{
                flex: 1,
                display: 'flex',
                justifyContent: 'flex-end',
                alignItems: 'center',
                "& .fa":{
                    color: '#d35400'
                }
            },
            '& h1':{
                fontSize: 18,
                color: '#444',
                textTransform: 'capitalize'
            }
        },
        "& .trend-desc":{
            '& p':{
                fontSize: '1em',
                color: '#636e72'
            }
        },
    }
}