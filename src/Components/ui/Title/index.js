import React, { Component } from 'react'
import injectSheet from 'react-jss'
import style from './style'

@injectSheet(style)
export default class Title extends Component {
  render() {
      let { classes, title, fontType } = this.props;
    return (
        <div className={classes.rootTitle}>
            <div className="list-title-wrap">
                <span className="trend-icon">
                    <i className={"fa " + fontType} aria-hidden="true"></i>
                </span>
                <h1>{title}</h1>
            </div>      
        </div>
    )
  }
}
