export default {
    rootTitle:{
            "& .list-title-wrap":{
                display: 'flex',
                padding: '20px 35px',
                background: '#fff',
                borderBottom: '1px solid #d3d3d3',
                alignItems: 'center',
                "& .trend-icon":{
                    paddingRight: 15,
                    "& .fa":{
                        fontSize: '1.3em',
                        color: '#1abc9c'
                    }
                },
                "& h1":{
                    margin: 0,
                    color: '#444',
                    fontSize: '1.4em'
                
                }
            }        
    }
}