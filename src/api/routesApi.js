const BASE_API_URL = process.env == "development" ? "http://localhost:8080/monitoring-project/api/" : "/monitoring-project/api/";
const GOOGLE_API_KEY = "AIzaSyCtjujusuw7_7v5Fnfe6VWLE48a2Yc2ofk";

const endpointPrefix = {
    login: 'login',
    region: 'region',
    trending: 'trending',
    resultMessage: 'resultmessage',
    alertMessage: 'alertmessage',
    alertDetails: 'alertdetails'
}

const apiRoutes = {
    login: BASE_API_URL + endpointPrefix.login,
    region: BASE_API_URL + endpointPrefix.region,
    trending: BASE_API_URL + endpointPrefix.trending,
    resultMessage: BASE_API_URL + endpointPrefix.resultMessage,
    alertMessage: BASE_API_URL + endpointPrefix.alertMessage,
    alertDetails: BASE_API_URL + endpointPrefix.alertDetails
}

export default apiRoutes


