
export const fetchData = (url) => {
    return new Promise((resolve, reject) => {
            fetch(url)
            .then(data => {
                return data.json()
            })
            .then(data => {
                resolve(data)
            })
            .catch((err) => {
                reject('Error while fetching data ', err);
            })        
    })
}